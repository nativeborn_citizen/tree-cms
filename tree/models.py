#coding: utf-8

from django.db import models
from django.contrib.flatpages.models import FlatPage

from mptt.models import MPTTModel, TreeForeignKey


class MenuItem(MPTTModel):
    class Meta:
        verbose_name_plural = 'Каталог'

    name = models.CharField(max_length=50, blank=True, null=True)
    page = models.ForeignKey(FlatPage, blank=True, null=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()
