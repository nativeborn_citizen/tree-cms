#coding: utf-8
from django.shortcuts import render

from .models import MenuItem


def show_top_menu(request):
    items = MenuItem.objects.filter(level=0)
    return render(request, 'base.html', {'items': items})
    
def show_menu(request, id):
    item = MenuItem.objects.get(pk=id)
    items = MenuItem.objects.filter(parent_id=id)
    return render(request, 'base.html', {'item': item,'items': items})
